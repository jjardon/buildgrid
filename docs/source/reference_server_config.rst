.. _parser:

Server configuration reference
==============================

BuildGrid's server configuration. To be used with::

  bgd server start server.conf

.. automodule:: buildgrid._app.settings.parser
    :members:
    :undoc-members:
    :show-inheritance:
